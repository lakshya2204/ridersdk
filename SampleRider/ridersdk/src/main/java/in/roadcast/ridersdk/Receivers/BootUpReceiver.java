package in.roadcast.ridersdk.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import in.roadcast.ridersdk.Helper.HelperClient;
import in.roadcast.ridersdk.Managers.RealmManager;
import in.roadcast.ridersdk.Managers.SessionManager;
import in.roadcast.ridersdk.Utils.MyConstants;


public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(this.getClass().getSimpleName(),"In On Receive method.");

        SessionManager sessionManager = HelperClient.getSessionManager(context);

        if(sessionManager.isAuthentified()) {
//            HelperClient.getCurrentLocation(context).getSingleLocation();
            RealmManager.getInstance(context).writeUserActivities(MyConstants.ACTION_TYPE_REBOOT,"1", sessionManager.getApiKey());
        }
    }

}
