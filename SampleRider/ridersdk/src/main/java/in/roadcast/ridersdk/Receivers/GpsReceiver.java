package in.roadcast.ridersdk.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import in.roadcast.ridersdk.Helper.HelperClient;
import in.roadcast.ridersdk.Managers.RealmManager;
import in.roadcast.ridersdk.Managers.SessionManager;
import in.roadcast.ridersdk.RoadcastDelegate;
import in.roadcast.ridersdk.locationHelper.ForegroundServiceLauncher;


public class GpsReceiver extends BroadcastReceiver {

    public GpsReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

        SessionManager sessionManager = HelperClient.getSessionManager(context);
        String gpsStatus=null;
        String gpsEvent="";

        if(sessionManager.isAuthentified())
        {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                gpsStatus="0";
                gpsEvent = "gps_off";
            }
            else if(lm.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {

                if (sessionManager.get_duty_status().equals("true"))
                {
                    ForegroundServiceLauncher.getInstance().startService(context, new RoadcastDelegate() {
                        @Override
                        public void success() {
                        }

                        @Override
                        public void failure(String error) {
                        }
                    });
                }
                gpsStatus="1";
                gpsEvent = "gps_on";
            }

            HelperClient.getCurrentLocation(context).getSingleLocation();
            RealmManager.getInstance(context).writeUserActivities(gpsEvent, gpsStatus, sessionManager.getApiKey());


        }
    }

}
